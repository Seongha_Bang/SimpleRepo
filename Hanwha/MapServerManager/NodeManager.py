import rosnode
import roslaunch
from fnmatch import fnmatch


class RosNode:
    def __init__(self, package, node_type, name=None, namespace='/', machine_name=None, args='', respawn=False,
                 respawn_delay=0.0, remap_args=None, env_args=None, output=None, cwd=None, launch_prefix=None,
                 required=False, filename='<unknown>'):
        self.node = roslaunch.Node(package=package, node_type=node_type, name=name, namespace=namespace,
                                   machine_name=machine_name, args=args, respawn=respawn, respawn_delay=respawn_delay,
                                   remap_args=remap_args, env_args=env_args, output=output, cwd=cwd,
                                   launch_prefix=launch_prefix, required=required, filename=filename)
        self.script = None

    def launch(self):
        launch = roslaunch.scriptapi.ROSLaunch()
        launch.start()
        self.script = launch.launch(self.node)

    def stop(self):
        self.script.stop()


def GetNodeStatus(name):
    result = False

    for node in rosnode.get_node_names():
        if fnmatch(node, name):
            result = True

    return result


def KillNodes(names):
    result = list()
    node_list = rosnode.get_node_names()

    if type(names) is str:
        names = [names]

    for node in node_list:
        for target in names:
            if fnmatch(node, target):
                if node not in result:
                    result.append(node)

    rosnode.kill_nodes(result)
    return result
