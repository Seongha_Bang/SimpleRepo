import ImageCommunication.Communication as MQTT
import ImageCommunication.MapDataCoding as MAP
import NodeManager as NM
import os
import yaml
from time import sleep


def send(file_dir, file_name, server_restart=True):
    DIR = os.path.dirname(os.path.abspath(__file__))

    mqtt = MQTT.Server(broker='127.0.0.1', port=1883, topic="/robot/mapdata")
    mqtt.start()

    data = MAP.GetEncodedMap(file_dir, file_name)
    data["restart"] = server_restart

    if data["error"] == 0:
        mqtt.publish(data)
    else:
        print("Error")


def receive(save_dir, file_name):
    DIR = os.path.dirname(os.path.abspath(__file__))

    mqtt = MQTT.Client(broker='127.0.0.1', port=1883, topic="/robot/mapdata")
    mqtt.start()

    while True:
        while mqtt.rcv_data is None:
            sleep(0.1)

        data = mqtt.GetReceivedData()

        yaml_data = yaml.safe_load(data["yaml"])
        yaml_data["image"] = file_name + ".png"
        data["yaml"] = yaml.safe_dump(yaml_data)

        MAP.SaveDecodedMap(data, save_dir, file_name)

        if data["restart"]:
            if NM.GetNodeStatus("/map_server*"):
                NM.KillNodes("/map_server*")

            NM.RosNode("map_server", "map_server", args=os.path.join(save_dir, file_name + ".yaml")).launch()


if __name__ == "__main__":
    receive("/home/dogu/dogu_system", "rcv_map")
