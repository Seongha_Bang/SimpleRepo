import base64


def ImageToStr(image_file):
    with open(image_file, 'rb') as f:
        image_byte = base64.b64encode(f.read())

    image_str = image_byte.decode('ascii')  # byte to str

    return image_str


def StrToImage(image_str, new_file):
    image_str = image_str.encode('ascii')
    image_byte = base64.b64decode(image_str)

    image_file = open(new_file, 'wb')
    image_file.write(image_byte)
    image_file.close()
