from Communication import Server, Client
from MapDataCoding import GetEncodedMap, SaveDecodedMap
from time import sleep


def server(mqtt, dir, start=False):
    if start:
        mqtt.start()

    data = GetEncodedMap(dir + "bigmap/", "map")
    mqtt.publish(data)


def client(mqtt, dir, start=False):
    buff = {"rcv_data": None}

    def on_message(client, userdata, msg):
        buff["rcv_data"] = msg.payload.decode()

    if start:
        mqtt.start(on_message)

    while buff["rcv_data"] is None:
        sleep(0.1)

    SaveDecodedMap(buff["rcv_data"], dir + "rcv", "rcv")


if __name__ == '__main__':
    DIR = "/home/dogu/BSH/pycharm-community-2020.2.3/workspace/ImageCommunication/"

    mqtt = {
        # "server": Server(broker='192.168.42.68', port=1883, topic="/robot/mapdata"),
        # "client": Client(broker='192.168.42.68', port=1883, topic="/robot/mapdata")
        "server": Server(broker='127.0.0.1', port=1883, topic="/robot/mapdata"),
        "client": Client(broker='127.0.0.1', port=1883, topic="/robot/mapdata")
    }

    print("Send data")
    server(mqtt["server"], DIR, start=True)

    print("Wait to rcv")
    client(mqtt["client"], DIR, start=True)

    sleep(0.5)

    print("Send data")
    server(mqtt["server"], DIR, start=False)
