from Communication import Server as MQTT
from PNGConverter import ImageToStr

if __name__ == '__main__':
    DIR = "/home/dogu/BSH/pycharm-community-2020.2.3/workspace/ImageCommunication/bigmap/"

    image_str = ImageToStr(DIR + "map.png")
    with open(DIR + "map.yaml", 'r') as f:
        yaml_str = f.read()

    mqtt = MQTT(broker='192.168.42.68', port=1883, topic="/robot/mapdata")
    client = mqtt.connect_mqtt()
    client.loop_start()

    mqtt.publish(client, {"image": image_str, "yaml": yaml_str})
