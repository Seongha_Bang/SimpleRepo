import base64
import random
import time
import json

from paho.mqtt import client as mqtt_client

broker = '192.168.42.68'
port = 1883
topic = "/robot/mapdata"

# broker = '10.0.2.15'
# port = 1883
# topic = "/robot/mapdata"

# generate client ID with pub prefix randomly
client_id = 'python-mqtt-{}'.format(random.randint(0, 1000))


def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish(client, data):
    msg = json.dumps({"messages": data})
    result = client.publish(topic, msg, qos=1)
    # result: [0, 1]
    print("Result: ", result)
    status = result[0]
    if status == 0:
        print("Send `{}` to topic `{}`".format(msg, topic))
    else:
        print("Failed to send message to topic {}".format(topic))


def imageToStr(image):
    with open(image, 'rb') as f:
        image_byte = base64.b64encode(f.read())
        print(type(image_byte))
    image_str = image_byte.decode('ascii')  # byte to str
    print(type(image_str))
    return image_str


def run():
    client = connect_mqtt()
    client.loop_start()
    publish(client, imageToStr("/home/dogu/BSH/pycharm-community-2020.2.3/workspace/ImageCommunication/bigmap/map.png"))


if __name__ == '__main__':
    run()
