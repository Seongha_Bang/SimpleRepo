from Communication import Client as MQTT
from PNGConverter import StrToImage
from time import sleep

if __name__ == '__main__':
    DIR = "/home/dogu/BSH/pycharm-community-2020.2.3/workspace/ImageCommunication/"

    mqtt = MQTT(broker='192.168.42.68', port=1883, topic="/robot/mapdata")
    client = mqtt.connect_mqtt()
    mqtt.subscribe(client)
    client.loop_start()

    while True:
        if mqtt.rcv_data is not None:
            print("rcv")

            StrToImage(mqtt.rcv_data["image"], DIR + "rcv.png")
            with open(DIR + "rcv.yaml", 'w') as f:
                yaml_str = f.write(mqtt.rcv_data["yaml"])

            mqtt.rcv_data = None
        sleep(0.1)

    # client.loop_forever()
