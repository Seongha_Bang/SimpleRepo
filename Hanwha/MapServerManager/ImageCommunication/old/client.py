import base64
import json
import random

from paho.mqtt import client as mqtt_client

broker = '192.168.42.68'
port = 1883
topic = "/robot/mapdata"

# broker = '192.168.111.3'
# port = 1883
# topic = "/robot/mapdata"

# generate client ID with pub prefix randomly
client_id = 'python-mqtt-{}'.format(random.randint(0, 100))


def connect_mqtt():  # mqtt_client
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client):
    def on_message(client, userdata, msg):
        rcv_data = json.loads(msg.payload.decode())["messages"]
        print(rcv_data)

        strToImage(rcv_data,
                   "/home/dogu/BSH/pycharm-community-2020.2.3/workspace/ImageCommunication/map/rcv.png")

    client.subscribe(topic)
    client.on_message = on_message


def strToImage(str, filename):
    image_str = str.encode('ascii')
    image_byte = base64.b64decode(image_str)
    image_json = open(filename, 'wb')
    image_json.write(image_byte)
    image_json.close()


def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()


if __name__ == '__main__':
    run()
