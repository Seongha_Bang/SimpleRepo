from PNGConverter import ImageToStr, StrToImage
import os


def GetEncodedMap(dir, name):
    result = {
        "image": "",
        "yaml": "",
        "error": 0
    }

    if os.path.isdir(dir):
        path = {
            "image": os.path.join(dir, name + ".png"),
            "yaml": os.path.join(dir, name + ".yaml")
        }

        if os.path.isfile(path["image"]) and os.path.isfile(path["yaml"]):
            result["image"] = ImageToStr(dir + name + ".png")
            with open(dir + "map.yaml", 'r') as f:
                result["yaml"] = f.read()
        else:
            result["error"] = -1
    else:
        result["error"] = -1

    return result


def SaveDecodedMap(data, dir, name):
    if not os.path.isdir(dir):
        os.makedirs(dir)

    StrToImage(data["image"], os.path.join(dir, name + ".png"))
    with open(os.path.join(dir, name + ".yaml"), 'w') as f:
        f.write(data["yaml"])
