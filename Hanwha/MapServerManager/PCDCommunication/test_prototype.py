from Communication import Server, Client
from os.path import join
from time import sleep, time
import open3d as o3d
import numpy as np

import json


def server(mqtt, dir, file, start=False):
    if start:
        mqtt.start()

    raw = o3d.io.read_point_cloud(join(dir, file)).points
    data = np.asarray(raw)
    mqtt.publish({"pcd": data.tolist(), "id": 123})


def client(mqtt, dir, file, start=False):
    buff = {"rcv_time": 0.0, "save": False, "data": None}

    def on_message(client, userdata, msg):
        print("start on_message")
        buff["rcv_time"] = time()

        rcv_data = mqtt.decode_message(msg)

        with open(join(dir, file), "w") as f:
            f.write(rcv_data)
        print("data saved")

        buff["data"] = json.loads(rcv_data)
        print("data loaded")

        buff["save"] = True

    if start:
        mqtt.start(on_message)

    while not buff["save"]:
        sleep(0.1)
    print("complete! - save time: {}, id: {}".format(str(time() - buff["rcv_time"]), buff["data"]["id"]))


if __name__ == '__main__':
    dir = "/home/dogu/BSH/pycharm-community-2020.2.3/workspace/_Git/Hanwha/MapServerManager/PCDCommunication/file"

    mqtt = {
        "server": Server(broker='192.168.50.68', port=1883, topic="/robot/pcddata"),
        "client": Client(broker='192.168.50.68', port=1883, topic="/robot/pcddata")
        # "server": Server(broker='127.0.0.1', port=1883, topic="/robot/pcddata"),
        # "client": Client(broker='127.0.0.1', port=1883, topic="/robot/pcddata")
    }

    # print("Send data")
    # server(mqtt["server"], dir, "office_212.pcd", start=True)
    #
    # sleep(0.5)

    print("Wait to rcv")
    client(mqtt["client"], dir, "rcv.json", start=True)

    # sleep(0.5)
    #
    # print("Send data")
    # server(mqtt["server"], dir, "office_212.pcd", start=False)
