import copy
import random
import json
from time import sleep
from paho.mqtt import client as mqtt_client


class Server:
    def __init__(self, broker, port, topic):
        self.broker = broker
        self.port = port
        self.topic = topic

        self.client_id = 'python-mqtt-{}'.format(random.randint(0, 1000))
        self.client = self.connect_mqtt()

    def connect_mqtt(self):
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        client = mqtt_client.Client(self.client_id)
        client.on_connect = on_connect
        client.connect(self.broker, self.port)
        return client

    def start(self):
        self.client.loop_start()

    def publish(self, msg):
        result = self.client.publish(self.topic, json.dumps(msg), qos=1)

        if result[0] == 0:
            result.wait_for_publish()
        else:
            print("Failed to send messGetReceivedDataage to topic {}".format(self.topic))


class Client:
    def __init__(self, broker, port, topic):
        self.broker = broker
        self.port = port
        self.topic = topic

        self.client_id = 'python-mqtt-{}'.format(random.randint(0, 100))

        self.client = self.connect_mqtt()

    def connect_mqtt(self):
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        client = mqtt_client.Client(self.client_id)
        client.on_connect = on_connect
        client.connect(self.broker, self.port)
        return client

    # def on_message(self, client, userdata, msg):
    #     msg.payload.decode()

    def decode_message(self, msg):
        # return json.loads(msg.payload.decode())
        return msg.payload.decode()

    def subscribe(self, client, on_message_func):
        client.subscribe(self.topic)
        client.on_message = on_message_func

    def start(self, on_message_func):
        self.client.loop_start()
        self.subscribe(self.client, on_message_func)
