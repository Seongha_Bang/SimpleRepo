#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import open3d as o3d
import numpy as np


class ConvertPCD2Map:
    def __init__(self, file_path):
        self.pcd_file_path = file_path
        # check local folder is exist or not
        if os.path.exists(self.pcd_file_path):
            self.points_data = self.load_pcd()
        else:
            self.points_data = []

    def load_pcd(self):
        """
        load pcd file
        """
        # load point cloud data
        pcd = o3d.io.read_point_cloud(self.pcd_file_path)
        points_data = np.asarray(pcd.points)
        return points_data
