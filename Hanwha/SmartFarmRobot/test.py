# -*- coding: utf-8 -*-

from JobFunctions import Job

block = Job("http://192.168.42.68:1337/")

if __name__ == '__main__':
    while True:
        block.Move("Container1-1")

        block.SetRegister(12, 1)
        block.SetRegister(1, 2)

        block.WaitRegister(1, 4)

        if block.GetRegister(3) != 0:
            block.MsgBox("작업이 실패하였습니다.")
            block.Pause()

        block.SetRegister(1, 1)
