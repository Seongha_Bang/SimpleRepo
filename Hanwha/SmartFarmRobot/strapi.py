import requests
import json


class strapi:
    def __init__(self, url):
        self.url = url
        self.headers = {"Content-Type": "application/json"}

        if self.url[-1] != '/':
            self.url += '/'

    def get_data(self, register):
        return requests.get(self.url + register).json()

    def put_data(self, register, key, value):
        requests.put(self.url + register, data=json.dumps({key: value}), headers=self.headers)
