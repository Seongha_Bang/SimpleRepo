# coding=utf-8
from time import sleep
from strapi import strapi

# num_to_key = {
#     1: "Operation_State",
#     2: "Task_Num",
#     3: "Current_State",
#     10: "Container_Num",
#     11: "Task_State",
#     12: "Line_Num",
#     13: "Sequence_Num"
# }

# key_to_register = {
#     "Operation_State": "registers-01",
#     "Task_Num": "registers-02",
#     "Current_State": "register-03",
#     "Container_Num": "register-10",
#     "Task_State": "register-11",
#     "Line_Num": "register-12",
#     "Sequence_Num": "register-13"
# }

register_name = "registers/"


class Job:
    def __init__(self, url):
        self.api = strapi(url)

    def SetRegister(self, num, value):
        print('{} 레지스터를 {}로 설정합니다.'.format(num, value))
        self.api.put_data(register_name + str(num), "value", value)

    def GetRegister(self, num):
        return self.api.get_data(register_name + str(num))["value"]

    def WaitRegister(self, num, value):
        print('{} 레지스터가 {}가 될 때까지 대기합니다.'.format(num, value))
        while self.api.get_data(register_name + str(num))["value"] != value:
            sleep(0.2)

    def Move(self, target):
        print('"{}"으로 이동합니다.'.format(target))

    def MsgBox(self, string):
        print(string)

    def Pause(self):
        raw_input()
