import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.getcwd() + "/tts-key.json"
from google.cloud import texttospeech
from pydub import AudioSegment

class TTS:
    def __init__(self, workspace):
        self.workspace = workspace

    def CreateFile(self, string, file_name="output"):
        client = texttospeech.TextToSpeechClient()

        synthesis_input = texttospeech.types.SynthesisInput(text=string)
        voice = texttospeech.types.VoiceSelectionParams(
            language_code="ko-KR", ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
        )
        audio_config = texttospeech.types.AudioConfig(
            audio_encoding=texttospeech.enums.AudioEncoding.MP3
        )

        response = client.synthesize_speech(synthesis_input, voice, audio_config)

        file_name = {
            "mp3": os.path.join(self.workspace, file_name + ".mp3"),
            "wav": os.path.join(self.workspace, file_name + ".wav")
        }

        with open(file_name["mp3"], "wb") as out:
            out.write(response.audio_content)

        sound = AudioSegment.from_mp3(file_name["mp3"])
        sound.export(file_name["wav"], format="wav")

        if os.path.isfile(file_name["mp3"]):
            os.remove(file_name["mp3"])
