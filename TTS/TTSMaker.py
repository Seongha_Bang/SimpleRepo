import os

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.getcwd() + "/tts-key.json"

from google.cloud import texttospeech


def Create_TTS_File(string, name):
    client = texttospeech.TextToSpeechClient()

    synthesis_input = texttospeech.types.SynthesisInput(text=string)
    voice = texttospeech.types.VoiceSelectionParams(
        language_code="ko-KR", ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
    )
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3
    )

    response = client.synthesize_speech(synthesis_input, voice, audio_config)

    with open(name + ".mp3", "wb") as out:
        out.write(response.audio_content)


if __name__ == '__main__':
    while True:
        Create_TTS_File(raw_input("String: "), raw_input("Name: "))
        print("")
