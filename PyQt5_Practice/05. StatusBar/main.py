import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton


class MyApp(QMainWindow):

    def __init__(self):
        super().__init__()

        self.count = 0

        self.initUI()

    def initUI(self):
        self.statusBar().showMessage('Ready')

        clickBtn = QPushButton('Count', self)
        clickBtn.move(50, 50)
        clickBtn.resize(clickBtn.sizeHint())
        clickBtn.clicked.connect(self.btn_count)

        clearBtn = QPushButton('Clear', self)
        clearBtn.move(50, 100)
        clearBtn.resize(clearBtn.sizeHint())
        clearBtn.clicked.connect(self.btn_clear)

        self.setWindowTitle('Statusbar')
        self.setGeometry(300, 300, 300, 200)
        self.show()

    def btn_count(self):
        self.count += 1
        self.statusBar().showMessage('Clicked ' + str(self.count))

    def btn_clear(self):
        self.count = 0
        self.statusBar().showMessage('Ready')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyApp()
    sys.exit(app.exec_())